# plover-dict-awesomewm

  This is my [Plover](https://www.openstenoproject.org/plover/)
  dictionary for steno chords supporting
  [awesome window manager](https://awesomewm.org).

  A left hand side prefix of `STPHR` is used for every window manager
  specific command.  It is also used as a lonely press prefix chord for
  closing windows or selecting screens or moving to screens by number.

# Dictionary

  Add `./dicts/awesome.json` to your dictionary list in plover.

# Cheatsheet

  The chords are documented in the [cheatsheet](./cheatsheet/steno-awesomewm-cheatsheet.pdf).

# License

  You can use all files in this repo respecting the CC BY-NC-SA Version 4.0.
  See [LICENSE](./LICENSE) file.


